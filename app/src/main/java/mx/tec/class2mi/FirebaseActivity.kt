package mx.tec.class2mi

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.EditText
import android.widget.Toast
import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.DocumentChange
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase

class FirebaseActivity : AppCompatActivity() {

    lateinit var email : EditText
    lateinit var password : EditText
    lateinit var nombre : EditText
    lateinit var edad : EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_firebase)

        email = findViewById(R.id.firebase_email)
        password = findViewById(R.id.firebase_password)
        nombre = findViewById(R.id.firebase_nombre)
        edad = findViewById(R.id.firebase_edad)

    }

    // registrar usuario con firebase
    fun registro(view : View?){

        Firebase.auth.createUserWithEmailAndPassword(
            email.text.toString(),
            password.text.toString()
        ).addOnCompleteListener(this){

                if(it.isSuccessful){

                    Log.d("FIREBASE", "Registro exitoso")
                } else {

                    Log.e("FIREBASE", "Registro fracasó: ${it.exception?.message}")
                }
        }
    }

    fun login(view : View?){

        Firebase.auth.signInWithEmailAndPassword(
            email.text.toString(),
            password.text.toString()
        ).addOnCompleteListener(this){

            if(it.isSuccessful)
                Log.d("FIREBASE", "Login exitoso")
            else
                Log.e("FIREBASE", "Login fracasó: ${it.exception?.message}")
        }
    }

    fun logout(view : View?){

        Firebase.auth.signOut()
    }

    fun verificarUsuario(){

        if(Firebase.auth.currentUser == null)
            Toast.makeText(this, "SIN USUARIO", Toast.LENGTH_SHORT).show()
        else
            Toast.makeText(this, Firebase.auth.currentUser?.email, Toast.LENGTH_SHORT).show()
        // este ejemplo sólo muestra un TOAST PERO
        // deberíamos redireccionar o actualizar GUI de acuerdo a la lógica de nuestro proyecto
    }

    fun verificarUsuarioGUI(view : View?){

        verificarUsuario()
    }

    override fun onStart(){
        super.onStart()

        verificarUsuario()
        // verificar siempre que la actividad vuelva a correr
    }


    fun registrarDatos(view : View?){

        // 1ero - crear un hashmap
        val gatito = hashMapOf(
            "nombre" to nombre.text.toString(),
            "edad" to edad.text.toString().toInt()
        )

        Firebase.firestore.collection("gatitos")
            .add(gatito)
            .addOnSuccessListener {

                Log.d("FIREBASE", "id: ${it.id}")
            }
            .addOnFailureListener{

                Log.e("FIREBASE", "excepcion: ${it.message}")
            }
    }

    fun leerDatos(view : View?) {

        // 1era - query "normal", solicitud de 1 vez

        Firebase.firestore.collection("gatitos")
            .get()
            .addOnSuccessListener {

                // recorrer query snapshot con un foreach
                for(documento in it){

                    Log.d("FIRESTORE", "${documento.id} ${documento.data}")
                }
            }
            .addOnFailureListener{
                Log.e("FIRESTORE", "error al leer gatitos: ${it.message}")
            }

        // 2da - updates en tiempo real
        Firebase.firestore.collection("gatitos")
            .addSnapshotListener{ datos, e ->

                // si hay error terminar ejecución
                if(e != null){

                    Log.e("FIRESTORE", "error: $e")
                    return@addSnapshotListener
                }

                // recorrer el snapshot
                for(cambio in datos!!.documentChanges){

                    when(cambio.type){

                        DocumentChange.Type.ADDED -> Log.d("FIRESTORE", "added: ${cambio.document.data}")
                        DocumentChange.Type.MODIFIED -> Log.d("FIRESTORE", "modified: ${cambio.document.data}")
                        DocumentChange.Type.REMOVED -> Log.d("REMOVED", "removed: ${cambio.document.data}")
                    }
                }
            }

    }
}