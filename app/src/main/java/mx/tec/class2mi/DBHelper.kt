package mx.tec.class2mi

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper


// SQLite - local relational database manager
// must extend sqlite dbhelper
class /* this is definition */ DBHelper(context : Context?) :
    /* this is invokation */ SQLiteOpenHelper(context, DB_FILE, null, 1) {

    override fun onCreate(db: SQLiteDatabase?) {

        // once when creating the file
        val query = "CREATE TABLE $TABLE(" +
                    "$COLUMN_ID INTEGER PRIMARY KEY, " +
                    "$COLUMN_NAME TEXT, " +
                    "$COLUMN_AGE INTEGER)"

        db?.execSQL(query)
    }

    override fun onUpgrade(db: SQLiteDatabase?, previousVersion: Int, currentVersion: Int) {

        // whenever version changed
        // do any maintenance in order to update your db

        // ? - wildcard (comodín)
        // prepared statement - parametrized query
        // query
        val query = "DROP TABLE IF EXISTS ?"
        val args = arrayOf(TABLE)

        db?.execSQL(query, args)
        onCreate(db)
    }

    fun save(name : String, age : Int) {

        // add new record (row) with information of a particular kitten
        // add args as values
        val values = ContentValues()
        values.put(COLUMN_NAME, name)
        values.put(COLUMN_AGE, age)

        // we're gonna use the "insert" method
        // this (along with others) are convenience methods
        writableDatabase.insert(TABLE, null, values)

    }

    fun delete(name : String) : Int {

        // why return a integer?
        // delete tells us how many rows where affected

        // we need a clause
        // clause - criteria used to decide which rows will be deleted
        val clause = "$COLUMN_NAME = ?"
        val args = arrayOf(name)

        return writableDatabase.delete(TABLE, clause, args);
    }

    fun find(name : String) : Int {

        // search a record using a name
        // return the id in that record

        // we need a clause too
        val clause = "$COLUMN_NAME = ?"
        val args = arrayOf(name)

        // retrieve a set from the database
        // cursor - iterator that tells us in which record from the result set
        // we are currently standing
        val cursor = readableDatabase.query(TABLE, null, clause, args, null, null, null)

        var result = -1

        // we achieve 2 things:
        // - 1st check if set is not empty
        // - actually move to the first
        if(cursor.moveToFirst())
            result = cursor.getInt(2)

        // how to traverse the set
        while(cursor.moveToNext()){
            // do something in every row
        }

        return result;
    }



    // new thing here
    // companion object
    companion object {

        private const val DB_FILE = "Kittens.db"
        private const val TABLE = "Kittens"
        private const val COLUMN_ID = "id"
        private const val COLUMN_NAME = "nombre"
        private const val COLUMN_AGE = "edad"

    }
}