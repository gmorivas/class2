package mx.tec.class2mi

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts

class MainActivity : AppCompatActivity() {

    // declare attributes
    // 2 choices
    lateinit var textView : TextView
    lateinit var editText : EditText
    lateinit var button : Button

    val activityResultLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
        result ->

        // always do this
        if(result.resultCode == Activity.RESULT_OK){

            // if everything was ok retrieve intent
            val data : Intent? = result.data

            Toast.makeText(this, data?.getStringExtra("result"), Toast.LENGTH_SHORT).show();
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        textView = findViewById(R.id.textView)
        editText = findViewById(R.id.editText)
        button = findViewById(R.id.button2)

        textView.setText("TEST FROM CODE")

        Toast.makeText(this, textView.getText().toString(), Toast.LENGTH_SHORT).show()

        Log.d("TESTLOG", "debug")
        Log.i("TESTLOG", "info")
        Log.w("TESTLOG", "warning")
        Log.e("TESTLOG", "error")
        Log.wtf("TESTLOG", "what a terrible failure!")

        // how to listen for a click
        // 2 ways
        // - through a listener
        // - linking through layout


        // - through a listener
        button.setOnClickListener {

            Toast.makeText(this, "button works.", Toast.LENGTH_SHORT).show()
        }
    }

    // - linking through layout
    public fun clicked(v : View?) {

        // we will change the activity here.
        // to launch an activity we request the OS to do so
        // to do so we use and intent (think of it as a request form)
        // (val means immutable variable)
        // 2 ways to open new activity:
        // - by explicit type
        // - by action i want to do
        val intent = Intent(this, SecondActivity::class.java)
        intent.putExtra("name", editText.getText().toString())


        //startActivity(intent)
        activityResultLauncher.launch(intent)
    }

    public fun clicked2(v: View?) {}

    public fun cambiarAFirebase(view : View?){

        val intent = Intent(this, FirebaseActivity::class.java)
        startActivity(intent)
    }
}